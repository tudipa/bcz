var NFAddressAutoomplete = Marionette.Object.extend( {

	initialize: function() {
		this.listenTo(nfRadio.channel('form'), 'render:view', this.maybeInitAutocomplete);
		this.listenTo(nfRadio.channel('nfMP'), 'change:part', this.maybeInitAutocomplete);
	},

	maybeInitAutocomplete: function() {
		jQuery('.nf-field-container.address-container .nf-element').each(function() {
			var $this = $(this);

			var autocomplete = $this.prop('_nf_autocomplete');
			if(!autocomplete) {
				autocomplete = new google.maps.places.Autocomplete(this);
				autocomplete.addListener('place_changed', function() {
					$this.change();
				});
			}
			
			$this.prop('_nf_autocomplete', autocomplete);
		});
	},

});

jQuery(document).ready(function($) {
	new NFAddressAutoomplete();
});