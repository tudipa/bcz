(function($) {
  "use strict"; // Start of use strict

  $("#navbarResponsive ul").addClass("navbar-nav text-uppercase ml-auto");
  $("#navbarResponsive ul li").addClass("nav-item");
  $("#navbarResponsive ul li a").addClass("nav-link js-scroll-trigger");

  var $win = $(window);
  $win.scroll(function () {
    if ($win.scrollTop() == 0) {
      var tweenTop2 = new TimelineMax();
      tweenTop2
      // .from('.masthead', 1, {y: '-10%', ease:Power0.easeNone}, 0)
      .from('.intro-lead-in', 1, {y: '-10%', autoAlpha: 0})
      .from('.intro-heading', 1, {y: '-10%', autoAlpha: 0})
      .from('.intro-btn', 1, {y: '-10%', autoAlpha: 0});
    }
  });

  var controller = new ScrollMagic.Controller();

  var tweenTop = new TimelineMax();
  tweenTop
    .from('.masthead', 1, {y: '-10%', ease:Power0.easeNone}, 0)
    .from('.intro-lead-in', 1, {y: '-10%', autoAlpha: 0})
    .from('.intro-heading', 1, {y: '-10%', autoAlpha: 0})
    .from('.intro-btn', 1, {y: '-10%', autoAlpha: 0});

  var tweenSvc = TweenMax.from('.svcr', 0.3, {autoAlpha: 0, scale: 0.5, ease:Linear.easeNone});
  var tweenPortr = TweenMax.from('.portr', 0.3, {autoAlpha: 0, scale: 0.5, ease:Linear.easeNone});
  var tweenAboutr = TweenMax.from('.aboutr', 0.3, {autoAlpha: 0, scale: 0.5, ease:Linear.easeNone});
  var tweenIphoner = TweenMax.from('.iphoner', 0.3, {autoAlpha: 0, scale: 0.5, ease:Linear.easeNone});
  var tweenTeamr = TweenMax.from('.teamr', 0.3, {autoAlpha: 0, scale: 0.5, ease:Linear.easeNone});

  var sceneSvc = new ScrollMagic.Scene({
    triggerElement: ".svcr",
    triggerHook: 1,
    duration: 300
  })
  .setTween(tweenSvc)
  .addTo(controller);

  var scene2 = new ScrollMagic.Scene({
    triggerElement: ".portr",
    triggerHook: 1,
    duration: 300
  })
  .setTween(tweenPortr)
  .addTo(controller);

  var scene3 = new ScrollMagic.Scene({
    triggerElement: ".aboutr",
    triggerHook: 1,
    duration: 300
  })
  .setTween(tweenAboutr)
  .addTo(controller);

  var scene4 = new ScrollMagic.Scene({
    triggerElement: ".iphoner",
    triggerHook: 1,
    duration: 300
  })
  .setTween(tweenIphoner)
  .addTo(controller);

  var scene5 = new ScrollMagic.Scene({
    triggerElement: ".teamr",
    triggerHook: 1,
    duration: 300
  })
  .setTween(tweenTeamr)
  .addTo(controller);

  $('.svcr1').each(function(){
    var tweenSvcr1 = TweenMax.from($(this), 0.3, {autoAlpha: 0, scale: 0.5, ease:Linear.easeNone});

    var sceneSvc = new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 1,
      duration: 300
    })
    .setTween(tweenSvcr1)
    .addTo(controller);
  })

  $('.portfolio-item').each(function(){
    var tweenPort = TweenMax.from($(this), 0.3, {autoAlpha: 0, scale: 0.5, ease:Linear.easeNone});

    var scene = new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 1,
      duration: 300
    })
    .setTween(tweenPort)
    .addTo(controller);
  })
  
  $('.fade-in').each(function(){
    var tweenTimeline = TweenMax.from($(this), 0.3, {autoAlpha: 0, scale: 0.5, ease:Linear.easeNone});

    var sceneTimeline = new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 1,
      duration: 300
    })
    .setTween(tweenTimeline)
    .addTo(controller);
  })

  $('.team-member').each(function(){
    var tweenTeam = TweenMax.from($(this), 0.3, {autoAlpha: 0, scale: 0.5, ease:Linear.easeNone});

    var sceneTeam = new ScrollMagic.Scene({
      triggerElement: this,
      triggerHook: 1,
      duration: 300
    })
    .setTween(tweenTeam)
    .addTo(controller);
  })

  var tlFirstScroll = new TimelineMax();
  tlFirstScroll
      .set('.iphone-image-wrapper', {scale: 2, transformOrigin: "center top"})
      .to('.iphone-image-wrapper', 3, {scale: 1.5, y: "-10%"})
      .to('.iphone-image-wrapper', 3, {scale: 1, y: "-40%"})

    var sceneApple = new ScrollMagic.Scene({
      triggerElement: '.trigger1',
      // triggerHook: 0,
      duration: "100%"
    })
    .setTween(tlFirstScroll)
    // .addIndicators()
    .addTo(controller);
  
var tlSecondScroll = new TimelineMax();

tlSecondScroll
.to('.iphone1', 3, {x: "-54%"})
.to('.iphone2', 3, {x: "54%"}, "-=3")
.from('.iphone1-text', 0.3, {autoAlpha: 0}, "-=3")
.from('.iphone2-text', 0.3, {autoAlpha: 0}, "-=3")
.to('.iphone1-text', 3, {x: "-30%"}, "-=3")
.to('.iphone2-text', 3, {x: "30%"}, "-=3")

.set('.iphone-stick', {display: "block"})    
.to('.iphone1-img-behind', 3, {x: "-54%"})
.to('.iphone2-img-behind', 3, {x: "54%"}, "-=3")

.to('.iphone1-img', 0.5, {autoAlpha: 0}, "-=3")
.to('.iphone2-img', 0.5, {autoAlpha: 0}, "-=3")

.to('.iphone1-text', 0.3, {autoAlpha: 0}, "-=3")
.to('.iphone2-text', 0.3, {autoAlpha: 0}, "-=3")

  var sceneApple2 = new ScrollMagic.Scene({
        triggerElement: '.trigger2',
        // triggerHook: 0,
        duration: "100%"
  })

  .setTween(tlSecondScroll)
  .setPin('.trigger2')
  // .addIndicators()
  .addTo(controller);
  // .addIndicators()
  // .addTo(controller);


  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 54)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 56
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);


  var ml = { timelines: {}};

  $('.ml7 .letters').each(function(){
    $(this).html($(this).text().replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>"));
  });
  
  ml.timelines["ml7"] = anime.timeline({loop: true})
    .add({
      targets: '.ml7 .letter',
      translateY: ["1.1em", 0],
      translateX: ["0.55em", 0],
      translateZ: 0,
      rotateZ: [180, 0],
      duration: 850,
      easing: "easeOutExpo",
      delay: function(el, i) {
        return 50 * i;
      }
    }).add({
      targets: '.ml7',
      opacity: 0,
      duration: 1000,
      easing: "easeOutExpo",
      delay: 1000
    });


})(jQuery); // End of use strict
