<!-- About -->
<section class="page-section" id="about">
    <div class="container">
      <div class="row aboutr">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">About</h2>
          <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <ul class="timeline">

          <?php
      $args = array(
	      'post_type' => 'about',
	      'posts_per_page' => 4,
	    );
      
      $the_query = new WP_Query( $args );
      if ( $the_query->have_posts() ) :
        $i = 1;
        while ( $the_query->have_posts() ) : $the_query->the_post();
        $about_years = get_field('about_years');
        
        $invert = "";
        if ($i % 2 == 0) $invert = "timeline-inverted";
        $i = $i + 1;

	    ?>

            <li class="fade-in <?php echo $invert; ?>">
              <div class="timeline-image">
                <img class="img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="">
              </div>
              <div class="timeline-panel">
                <div class="timeline-heading">
                  <h4><?php echo $about_years; ?></h4>
                  <h4 class="subheading"><?php the_title() ?></h4>
                </div>
                <div class="timeline-body">
                  <p class="text-muted"><?php the_content(); ?></p>
                </div>
              </div>
            </li>

            <?php endwhile; endif; wp_reset_postdata(); ?>
            
            <li class="fade-in timeline-inverted">
              <div class="timeline-image">
                <h4>Be Part
                  <br>Of Our
                  <br>Story!</h4>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>