<?php
      $args = array(
	      'post_type' => 'apple',
	      'posts_per_page' => 1
	    );
      
      $the_query = new WP_Query( $args );
      if ( $the_query->have_posts() ) :
        while ( $the_query->have_posts() ) : $the_query->the_post();
        $about_years = get_field('about_years');

        $about_img1 = get_field('image_1');
        $about_img2 = get_field('image_2');
        $about_img3 = get_field('image_3');

        $about_text1 = get_field('text_1');
        $about_text11 = get_field('text_11');
        $about_text2 = get_field('text_2');
        $about_text22 = get_field('text_22');

	    ?>

<section id="applep">
  <div class="trigger1 iphoner">
    <div>
      <h2 class="position-h2"><?php the_title(); ?></h2> 
    </div>
  </div>
</section>

  <section id="apple">
    <div class="trigger2">
        <div class="iphone-image-wrapper">
            <div class="iphone1-text">
                <p><?php echo $about_text1 ?></p>
                <p><?php echo $about_text11 ?></p>
            </div>
            <div class="iphone-image iphone1">
                <div class="inner-phone"> 
                <img class="iphone1-img" src="<?php echo $about_img1; ?>" alt=""> 
                <img class="iphone-stick" src="<?php echo $about_img3; ?>" alt=""> 
                <img class="iphone1-img-behind" src="<?php echo $about_img2; ?>" alt=""> </div>
            </div>
            <div class="iphone-image iphone2">
                <div class="inner-phone"> 
                <img class="iphone2-img" src="<?php echo $about_img1; ?>" alt=""> 
                <img class="iphone2-img-behind" src="<?php echo $about_img2; ?>" alt=""> 
                </div>
            </div>
            <div class="iphone2-text">
                <p><?php echo $about_text2 ?></p>
                <p><?php echo $about_text22 ?></p>
            </div>
        </div>
    </div>
</section>

<?php endwhile; endif; wp_reset_postdata(); ?>