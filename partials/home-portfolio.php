<!-- Portfolio Grid -->
<section class="bg-light page-section" id="portfolio">
  <div class="container">
      <div class="row portr">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Portfolio</h2>
          <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
        </div>
      </div>
      <div class="row">

      <?php
      $args = array(
	      'post_type' => 'portfolio',
	      'posts_per_page' => 6,
	    );
      
      $the_query = new WP_Query( $args );
      if ( $the_query->have_posts() ) :
        $i = 1;
        while ( $the_query->have_posts() ) : $the_query->the_post();
          $port_cat = get_field('port_category');
          $modal = "#portfolioModal".$i;
          $i = $i + 1;
	    ?>

        <div class="col-md-4 col-sm-6 portfolio-item">
          <a class="portfolio-link" data-toggle="modal" href="<?php echo $modal; ?>">
            <div class="portfolio-hover">
              <div class="portfolio-hover-content">
                <i class="fas fa-plus fa-3x"></i>
              </div>
            </div>
            
            <img class="img-fluid" alt="" src=<?php the_post_thumbnail_url(); ?>>
          </a>
          <div class="portfolio-caption">
            <h4><?php the_title(); ?></h4>
            <p class="text-muted"><?php echo $port_cat; ?></p>
          </div>
        </div>

        <?php endwhile; endif; wp_reset_postdata(); ?>

      </div>
  </div>
  </section>