<!-- Portfolio Modals -->

<?php
      $args = array(
	      'post_type' => 'portfolio',
	      'posts_per_page' => 6,
	    );
      
      $the_query = new WP_Query( $args );
      if ( $the_query->have_posts() ) :
        $i = 1;
        while ( $the_query->have_posts() ) : $the_query->the_post();
          $modal = "portfolioModal".$i;
          $port_cat = get_field('port_category');
          $port_img = get_field('port_full_image');
          $port_sub = get_field('port_subtitle');
          $port_date = get_field('port_date');
          $port_client = get_field('port_client');
          $i = $i + 1;
	    ?>

  <div class="portfolio-modal modal fade" id="<?php echo $modal; ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
          <div class="lr">
            <div class="rl"></div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <div class="modal-body">
                <!-- Project Details Go Here -->
                <h2 class="text-uppercase"><?php the_title(); ?></h2>
                <p class="item-intro text-muted"><?php echo $port_sub; ?></p>
                <img class="img-fluid d-block mx-auto" src="<?php echo $port_img; ?>" alt="">
                <p><?php the_content(); ?></p>
                <ul class="list-inline">
                  <li>Date: <?php echo $port_date; ?></li>
                  <li>Client: <?php echo $port_client; ?></li>
                  <li>Category: <?php echo $port_cat; ?></li>
                </ul>
                <button class="btn btn-primary" data-dismiss="modal" type="button">
                  <i class="fas fa-times"></i>
                  Close Project</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php endwhile; endif; wp_reset_postdata(); ?>