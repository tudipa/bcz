<!-- Team -->
<section class="page-section" id="team">
    <div class="container">
      <div class="row teamr">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Our Amazing Team</h2>
          <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
        </div>
      </div>
      <div class="row">

      <?php
      $args = array(
	      'post_type' => 'team',
	      'posts_per_page' => 3
	    );
      
      $the_query = new WP_Query( $args );
      if ( $the_query->have_posts() ) :
        $i = 1;
        while ( $the_query->have_posts() ) : $the_query->the_post();
        $twitter = get_field('twitter');
        $facebook = get_field('facebook');
        $linkedin = get_field('linkedin');
        $position = get_field('position');
	    ?>

        <div class="col-sm-4">
          <div class="team-member">
            <img class="mx-auto rounded-circle" src="<?php the_post_thumbnail_url(); ?>" alt="">
            <h4><?php the_title(); ?></h4>
            <p class="text-muted"><?php echo $position; ?></p>
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="<?php echo $twitter; ?>">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="<?php echo $facebook; ?>">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="<?php echo $linkedin; ?>">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>

      <?php endwhile; endif; wp_reset_postdata(); ?>

      </div>

      <div class="row">
        <div class="col-lg-8 mx-auto text-center">
          <p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
        </div>
      </div>
    </div>
  </section>