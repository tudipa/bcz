<!-- Services -->
  <section class="page-section" id="services">
    <div class="container">
      <div class="row svcr">
        <div class="col-lg-12 text-center">
          <h2 class="section-heading text-uppercase">Services</h2>
          <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
        </div>
      </div>


      <div class="row text-center">
      <?php
      $args = array(
	      'post_type' => 'services',
	      'posts_per_page' => 3,
	    );
      
      $the_query = new WP_Query( $args );
	    if ( $the_query->have_posts() ) :
        while ( $the_query->have_posts() ) : $the_query->the_post();
          $icon = get_field('svc-icon');
	    ?>

      
        <div class="col-md-4 svcr1">
          <span class="fa-stack fa-4x">
            <!-- <i class="bodymovin"></i> -->
            <i class="fas fa-circle fa-stack-2x text-primary"></i>
            <i class="<?php echo $icon; ?> fa-stack-1x fa-inverse"></i>
          </span>
          <h4 class="service-heading"><?php the_title(); ?></h4>
          <p class="text-muted"><?php the_content() ?></p>
        </div>
      <?php endwhile; endif; wp_reset_postdata(); ?>
      </div>

    </div>
  </section>