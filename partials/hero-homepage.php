<?php
$hero_image = get_field('homepage_hero_image','option');
$txt_0 = get_field('homepage_first_text','option');
$txt_1 = get_field('homepage_second_text','option');
$txt_2 = get_field('homepage_button_text','option');

// var_dump($txt_2);
?>

<!-- Header -->
<header class="masthead" style="background-image: url(<?php echo $hero_image; ?>)">
    <div class="container">
      <div class="intro-text">
        <div class="intro-lead-in"><?php echo $txt_0; ?></div>
        <div class="ml7">
          <span class="text-wrapper text-uppercase">
            <span class="letters"><?php echo $txt_1; ?></span>
          </span>
        </div>

        <a class="intro-btn btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services"><?php echo $txt_2; ?></a>
      </div>
    </div>
  </header>