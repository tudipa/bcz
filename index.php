<?php get_header(); ?>

<?php get_template_part('partials/hero', 'homepage'); ?>        
<?php get_template_part('partials/home', 'services'); ?>
<?php get_template_part('partials/home', 'portfolio'); ?>
<?php get_template_part('partials/home', 'about'); ?>
<?php get_template_part('partials/home', 'apple'); ?>
<?php get_template_part('partials/home', 'team'); ?>
<?php get_template_part('partials/home', 'modal'); ?>
<?php get_template_part('partials/home', 'contact-form'); ?>

<?php get_footer(); ?>