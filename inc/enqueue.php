<?php

function lp_enqueue_frontend() {

	if(!is_admin()):        
        wp_enqueue_style('bootstrap', get_bloginfo('template_url').'/dist/css/bootstrap.min.css');
        wp_enqueue_style('fontawesome', get_bloginfo('template_url').'/dist/css/all.min.css');
        wp_enqueue_style('googlefont-monts', 'https://fonts.googleapis.com/css?family=Montserrat:400,700');
        wp_enqueue_style('googlefont-kaush', 'https://fonts.googleapis.com/css?family=Kaushan+Script');
        wp_enqueue_style('googlefont-droid', 'https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic');
        wp_enqueue_style('googlefont-roboto', 'https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700');
        wp_enqueue_style('agency', get_bloginfo('template_url').'/dist/css/agency.css');

        wp_enqueue_script('jquery-tu', get_bloginfo('template_url').'/dist/js/jquery.min.js', [], null, true);
        wp_enqueue_script('bootstrap-tu', get_bloginfo('template_url').'/dist/js/bootstrap.bundle.min.js', [], null, true);
        wp_enqueue_script('jquery-easing', get_bloginfo('template_url').'/dist/js/jquery.easing.min.js', [], null, true);
        wp_enqueue_script('tweenmax', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenMax.min.js', [], null, true);
        wp_enqueue_script('scrollmagic', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js', [], null, true);
        wp_enqueue_script('gsap-animation', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js', [], null, true);
        wp_enqueue_script('add-indicators', 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/debug.addIndicators.min.js', [], null, true);
        wp_enqueue_script('lottie', 'https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.5.0/lottie.js', [], null, true);
        wp_enqueue_script('anime', 'https://cdnjs.cloudflare.com/ajax/libs/animejs/2.2.0/anime.js', [], null, true);
        wp_enqueue_script('bootstrap-validation', get_bloginfo('template_url').'/dist/js/jqBootstrapValidation.js', [], null, true);
		wp_enqueue_script('agency-tu', get_bloginfo('template_url').'/dist/js/agency.js', [], null, true);
		
		if(defined('LP_GMAPS_KEY') && LP_GMAPS_KEY) { wp_enqueue_script('gmaps_api', 'https://maps.googleapis.com/maps/api/js?key='.LP_GMAPS_KEY.'&libraries=places', '', '', true); }
			wp_enqueue_script('nf-addressautocomplete', get_template_directory_uri().'/ninja-forms/js/addressautocomplete.js', array('nf-front-end-deps'),'', true);
	endif;
}
add_action('wp_enqueue_scripts', 'lp_enqueue_frontend', 100);