<?php

function setup_acf() {
	// Hide from admin menu
	//acf_update_setting('show_admin', false);

	// Google maps
	if(defined('LP_GMAPS_KEY') && LP_GMAPS_KEY) {
		acf_update_setting('google_api_key', LP_GMAPS_KEY);
	}
}
add_action('acf/init', 'setup_acf');