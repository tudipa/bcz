<?php

require 'inc/constants.php';
require 'inc/acf.php';
require 'inc/enqueue.php';


function init() {
        // Remove Unnecessary tags
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_generator');
        // Register Navigation item
        register_nav_menus([
                'header_nav' => __('Primary Navigation tu')
        ]);
        // Enable thumbnails/featured image
        add_theme_support('post-thumbnails');
        // Add HTML5 markup for captions
        add_theme_support('html5', ['caption', 'comment-form', 'comment-list']);
}
add_action('after_setup_theme','init');


function my_acf_json_save_point( $path ) {
        $path = get_stylesheet_directory() . '/json-acf';
        return $path;
}
add_filter('acf/settings/save_json', 'my_acf_json_save_point');

if( function_exists('acf_add_options_page') ) {
	$parent = acf_add_options_page(array(
		'page_title' 	=> 'General Settings',
		'menu_title' 	=> 'General Settings',
		'redirect' 		=> false
	));
}

$p_types = array(
	'services' => array(
		'name' => 'Services',
		'icon' => 'dashicons-calendar-alt',
		'slug' => 'services',
		'gutenberg' => false
	),
	'portfolio' => array(
		'name' => 'Portfolio',
		'icon' => 'dashicons-megaphone',
		'slug' => 'portfolio',
		'gutenberg' => false
	),
	'about' => array(
		'name' => 'About',
		'icon' => 'dashicons-image-filter',
		'slug' => 'about',
		'gutenberg' => false
	),
	'apple' => array(
		'name' => 'Apple',
		'icon' => 'dashicons-clipboard',
		'slug' => 'apple',
		'gutenberg' => false
	),
	'team' => array(
		'name' => 'Team',
		'icon' => 'dashicons-format-chat',
		'slug' => 'team',
		'gutenberg' => false
	)
);

function create_post_type() {
	global $p_types;
	foreach($p_types as $key=>$type):
		register_post_type( $key,
			array(
                                'labels' => array(
                                'name' => __( $type['name'] ),
                                'singular_name' => __( $type['name'] )
                                ),
                                'public' => true,
                                'has_archive' => $type['gutenberg'],
                                'supports' => array( 'title', 'editor', 'thumbnail', 'revisions' ),
                                'menu_position' => 5,
                                'rewrite' => array( 'slug' => $type['slug'] ),
                                'menu_icon' => $type['icon'],
                                'show_in_rest' => $type['gutenberg']
			)
		);
	endforeach;
}
add_action( 'init', 'create_post_type' );

?>