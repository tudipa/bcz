<!-- Footer -->
<?php

$twitter = get_field("foot_twitter", "option");
$facebook = get_field("foot_facebook", "option");
$linkedin = get_field("foot_linkedin", "option");

?>

<footer class="bg-light footer">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-4">
          <span class="copyright">Copyright &copy; Adipa 2019</span>
        </div>
        <div class="col-md-4">
          <ul class="list-inline social-buttons">
            <li class="list-inline-item">
              <a target="_blank" href="<?php echo $twitter; ?>">
                <i class="fab fa-twitter"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a target="_blank" href="<?php echo $facebook; ?>">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a target="_blank" href="<?php echo $linkedin; ?>">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <ul class="list-inline quicklinks">
            <li class="list-inline-item">
              <a href="#">Privacy Policy</a>
            </li>
            <li class="list-inline-item">
              <a href="#">Terms of Use</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <?php wp_footer(); ?>

  </body>
</html>